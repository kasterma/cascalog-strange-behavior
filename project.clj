(defproject testcascalog "0.1.0-SNAPSHOT"
  :description "working to isolate strange cascalog behavior"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [cascalog/cascalog-core "2.1.1"]
                 [com.taoensso/timbre "3.2.1"]]
  :profiles { :dev {:dependencies [[org.apache.hadoop/hadoop-core "1.2.1"]]}}
  :jvm-opts ["-Xms768m" "-Xmx768m"])
