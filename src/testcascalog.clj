(ns testcascalog
  (:require [cascalog.api :refer :all]
            [taoensso.timbre :as log]))

(def dat [[1 1] [1 2] [1 3]])

(defbufferfn dosort [tuples]
  (log/log (class tuples))
  (sort-by first tuples))

(defbufferfn dosort-ev [tuples]
  (vec (sort-by first tuples)))

(comment
(def dosort
 (cascalog.logic.def/bufferop
   (clojure.core/with-meta
     (fn* ([tuples] (sort-by first tuples)))
     {:cascalog.logic.fn/fn-type :cascalog.logic.fn/serializable-fn,
      :cascalog.logic.fn/source
      "(cascalog.logic.fn/fn [tuples] ^#=(clojure.lang.PersistentArrayMap/create {:line #=(java.lang.Integer. \"2\"), :column #=(java.lang.Integer. \"3\")}) (sort-by first tuples))",
      :cascalog.logic.fn/namespace "testcascalog",
      :cascalog.logic.fn/env (clojure.core/into {} [])})))
)

(defbufferfn dosortv [tuples]
  (sort-by first (map vec tuples)))

(defbufferfn dosortl [tuples]
  (sort-by first (mapv identity tuples)))

(comment
  (def dosortv
    (cascalog.logic.def/bufferop
     (clojure.core/with-meta
       (fn* ([tuples] (sort-by first (map vec tuples))))
       {:cascalog.logic.fn/fn-type :cascalog.logic.fn/serializable-fn,
        :cascalog.logic.fn/source
        "(cascalog.logic.fn/fn [tuples] ^#=(clojure.lang.PersistentArrayMap/create {:line #=(java.lang.Integer. \"2\"), :column #=(java.lang.Integer. \"3\")}) (sort-by first ^#=(clojure.lang.PersistentArrayMap/create {:line #=(java.lang.Integer. \"2\"), :column #=(java.lang.Integer. \"18\")}) (map vec tuples)))",
        :cascalog.logic.fn/namespace "testcascalog",
        :cascalog.logic.fn/env (clojure.core/into {} [])})))
  )

(defn a []
  (??<- [?i ?s]
        (dat ?i ?x)
        (dosort ?x :> ?s)))

(defn c []
  (??<- [?i ?s]
        (dat ?i ?x)
        (dosort-ev ?x :> ?s)))


(comment
  (<- [?i ?s]
      (dat ?i ?x)
      (dosort ?x :> ?s))


(let*
  [?i "?i" ?s "?s" ?x "?x"]
  (cascalog.logic.parse/parse-subquery
    [?i ?s]
    [[dat ?i ?x] [dosort ?x :> ?s]]))
)

(defn b []
  (??<- [?i ?s]
        (dat ?i ?x)
        (dosortv ?x :> ?s)))

(defn d []
  (??<- [?i ?s]
        (dat ?i ?x)
        (dosortl ?x :> ?s)))

(comment
  (<- [?i ?s]
      (dat ?i ?x)
      (dosortv ?x :> ?s))

  (let*
   [?i "?i" ?s "?s" ?x "?x"]
   (cascalog.logic.parse/parse-subquery
    [?i ?s]
    [[dat ?i ?x] [dosortv ?x :> ?s]])))
